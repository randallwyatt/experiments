﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Experiments
{
    class AgeConvert
    {

       

        public int AgeConversionDays(int age)
        {
            Console.WriteLine("This was generated from the AgeConvert class.");
            int daysOld = age * 365;

            return daysOld;

        }

        public int AgeConversionSeconds(int age)
        {
            int secondsOld = age * 86400;

            return secondsOld;

        }

        public int AgeConversionMinutes(int age)
        {
            int minutesOld = age * 525600;

            return minutesOld;

        }

        public int AgeConversionHours(int age)
        {
            int hoursOld = age * 8760;

            return hoursOld;

        }
    }
}
